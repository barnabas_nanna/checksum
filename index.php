<?php
//set up autoloader
$loader = require __DIR__ . '/vendor/autoload.php';
$loader->setPsr4("lib\\", __DIR__ . "/lib");

$spreadsheet = array(
    [5, 1, 9, 5],
    [7, 5, 3],
    [2, 4, 6, 8]
);

echo (new \lib\CheckSumCalculator($spreadsheet))->getCheckSum(). PHP_EOL;