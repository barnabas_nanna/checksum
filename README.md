**Run application**

This is an application to calculate and display the checksum of a multi-dimensional array.
To run application, navigate to base path of application and run the command below.

$. php index.php

**Testing application**

To run unit test run command at base path of application.

$. vendor/bin/phpunit tests/lib/CheckSumCalculatorTest.php
