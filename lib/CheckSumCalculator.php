<?php
/**
 * Created by PhpStorm.
 * User: bnanna
 * Date: 11/01/2018
 * Time: 15:49
 */

namespace lib;

class CheckSumCalculator
{

    private $spreadsheet;
    protected $checksum = 0;

    public function __construct(array $spreadsheet)
    {
        $this->spreadsheet = $spreadsheet;
        $this->calculateCheckSum();
    }

    protected function calculateCheckSum()
    {
        foreach ($this->spreadsheet as &$row) {
            if (is_array($row)) {
                $row = array_filter($row, 'is_numeric');
                $this->checksum += max($row) - min($row);
            }
        }
        unset($row);
    }

    public function getCheckSum()
    {
        return $this->checksum;
    }
}
