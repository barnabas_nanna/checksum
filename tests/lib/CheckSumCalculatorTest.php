<?php

namespace tests;

$loader = require __DIR__ . '/../../vendor/autoload.php';
$loader->setPsr4("lib\\", __DIR__ . "/../../lib");


use \lib\CheckSumCalculator;
use \PHPUnit\Framework\TestCase;

class CheckSumCalculatorTest extends TestCase
{
    public function testCheckSum()
    {
        $spreadsheet = array(
            [5, 1, 9, 5],
            [7, 5, 3],
            [2, 4, 6, 8]
        );
        $checkSumCalculator = new CheckSumCalculator($spreadsheet);
        $result = $checkSumCalculator->getCheckSum();

        // assert that your checksum is as expected
        $this->assertEquals(18, $result);
    }

}
